# Mobile CRUD app (Shopping List) - SIP Test

Aplikasi mobile yang mengimplementasikan CRUD dengan framework flutter

## Tech Stack

**Database :** MySQL

**Backend :** Laravel 10

**Mobile :** Flutter, Dart

### Versi yang digunakan :

• Flutter version 3.7.12

• Dart version 2.19.6

• Laravel 10

## Installation

### Clone dan masuk ke repository

```bash
  git clone https://gitlab.com/sip-tests/mobile-crud.git
  cd mobile-crud
```

### Kemudian Install dependensi

```bash
  flutter pub get
  flutter run
```

- Kemudian pilih jenis device
