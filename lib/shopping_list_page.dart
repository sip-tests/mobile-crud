import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'config.dart';

class ShoppingListPage extends StatelessWidget {
  final TextEditingController namaController = TextEditingController();
  final TextEditingController catatanController = TextEditingController();
  final Function()? onDataSaved;

  ShoppingListPage({this.onDataSaved});

  Future<void> _simpanDataBelanja(BuildContext context) async {
    final url = Uri.parse('http://127.0.0.1:8000/shoppings');

    final response = await http.post(
      url,
      body: {
        'name': namaController.text,
        'description': catatanController.text,
      },
    );

    if (response.statusCode == 201) {
      print('Data belanja berhasil disimpan.');
      Navigator.of(context).pop();
      onDataSaved?.call();
    } else {
      print('Gagal menyimpan data belanja: ${response.body}');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Create Shopping List'),
        backgroundColor: Colors.black,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            TextField(
              controller: namaController,
              decoration: InputDecoration(labelText: 'Nama'),
            ),
            TextField(
              controller: catatanController,
              decoration: InputDecoration(labelText: 'Catatan'),
            ),
            SizedBox(height: 16.0),
            ElevatedButton(
              onPressed: () {
                _simpanDataBelanja(context);
              },
              style: ElevatedButton.styleFrom(
                primary: Colors.black,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.zero,
                ),
              ),
              child: Text('Simpan', style: TextStyle(color: Colors.white)),
            ),
          ],
        ),
      ),
    );
  }
}
