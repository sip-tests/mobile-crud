import 'package:flutter/material.dart';
import 'shopping_list_page.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'config.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Shopping List',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Shopping List'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Map<String, dynamic>> items = [];
  bool isDataSaved = false;
  String message = '';

  void reloadData() {
    fetchData();
  }

  // final List<Map<String, String>> items = [
  //   {"name": "Item 1", "description": "Description 1"},
  //   {"name": "Item 2", "description": "Description 2"},
  //   {"name": "Item 3", "description": "Description 3"},
  //   {"name": "Item 4", "description": "Description 4"},
  //   {"name": "Item 5", "description": "Description 5"},
  // ];
  int _counter = 0;

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  void _showSnackbar() {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
        backgroundColor: Colors.green,
      ),
    );
  }

  Future<void> fetchData() async {
    final response =
        await http.get(Uri.parse('http://127.0.0.1:8000/shoppings'));

    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      setState(() {
        items = List<Map<String, dynamic>>.from(data);
        if (isDataSaved) {
          _showSnackbar();
          isDataSaved = false;
          reloadData();
        }
      });
    } else {
      throw Exception('Failed to load data');
    }
  }

  void deleteItem(int id) async {
    final response =
        await http.delete(Uri.parse('http://127.0.0.1:8000/shoppings/$id'));

    if (response.statusCode == 200) {
      fetchData();
    } else {
      print('Gagal menghapus item. Status code: ${response.statusCode}');
    }
  }

  void editItem(int id, Map<String, dynamic> updatedData) async {
    final response = await http.put(
      Uri.parse('http://127.0.0.1:8000/shoppings/$id'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(updatedData),
    );

    if (response.statusCode == 200) {
      fetchData();
    } else {
      print('Failed to update item. Status code: ${response.statusCode}');
    }
  }

  void showDeleteConfirmationDialog(BuildContext context, int itemId) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Konfirmasi Hapus'),
          content: Text('Apakah Anda yakin ingin menghapus item ini?'),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop(); // Tutup dialog
              },
              child: Text('Batal'),
            ),
            TextButton(
              onPressed: () {
                deleteItem(itemId);
                Navigator.of(context).pop();
              },
              child: Text('Hapus'),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        backgroundColor: Colors.black,
      ),
      body: ListView.builder(
        itemCount: items.length,
        itemBuilder: (context, index) {
          final item = items[index];
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              child: ListTile(
                title: Text(item["name"]!),
                subtitle: Text(item["description"]!),
                trailing: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    IconButton(
                      icon: Icon(Icons.edit),
                      color: Colors.yellow,
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            final TextEditingController nameController =
                                TextEditingController(text: item["name"]);
                            final TextEditingController descriptionController =
                                TextEditingController(
                                    text: item["description"]);

                            return AlertDialog(
                              title: Text('Edit Item'),
                              content: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  TextField(
                                    controller: nameController,
                                    decoration:
                                        InputDecoration(labelText: 'Name'),
                                  ),
                                  TextField(
                                    controller: descriptionController,
                                    decoration: InputDecoration(
                                        labelText: 'Description'),
                                  ),
                                ],
                              ),
                              actions: <Widget>[
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop(); // Close dialog
                                  },
                                  child: Text('Cancel'),
                                ),
                                TextButton(
                                  onPressed: () {
                                    final updatedData = {
                                      "name": nameController.text,
                                      "description": descriptionController.text,
                                    };
                                    final itemId = item["id"];
                                    editItem(itemId, updatedData);
                                    Navigator.of(context).pop();
                                  },
                                  child: Text('Save'),
                                ),
                              ],
                            );
                          },
                        );
                      },
                    ),
                    IconButton(
                      icon: Icon(Icons.delete),
                      color: Colors.red,
                      onPressed: () {
                        final itemId = item["id"];
                        showDeleteConfirmationDialog(context, itemId);
                      },
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ShoppingListPage(onDataSaved: reloadData),
            ),
          );
        },
        backgroundColor: Colors.black,
        child: const Icon(Icons.add),
      ),
    );
  }
}
